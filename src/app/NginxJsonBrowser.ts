import {
  AbstractDirEntry,
  DirBaseEntry,
  DirBlobEntry,
  DirTreeEntry,
} from './DirEntry.js';
import { DirPath, FileBrowser } from './FileBrowser.js';
import { Consumer } from './Util.js';

export function createNginxJsonBrowser(baseUrl: DirPath): FileBrowser {
  return new FileBrowser(baseUrl, convert);
}

interface DirItem {
  readonly name: string;
  readonly type: 'file' | 'directory';
}

function isDirItem(obj: object): obj is DirItem {
  return (
    typeof obj['name'] === 'string' &&
    ['directory', 'file'].includes(obj['type'])
  );
}

class NginxEntry extends AbstractDirEntry {
  constructor(parent: DirTreeEntry, name: string) {
    super(parent, name);
  }
  getUrlPath(): string {
    return this.getPath().join('/') + '/';
  }
}

class NginxBlobEntry extends AbstractDirEntry implements DirBlobEntry {
  readonly type: 'blob' = 'blob';
  constructor(parent: DirTreeEntry, name: string) {
    super(parent, name);
  }
  getUrlPath(): string {
    return this.getPath().join('/');
  }
}

function convert(
  parent: DirTreeEntry,
  text: string,
  treeEntrySink: Consumer<DirBaseEntry>,
  blobEntrySink: Consumer<DirBlobEntry>
): void {
  const items = JSON.parse(text);
  if (!Array.isArray(items)) {
    return;
  }
  for (const item of items) {
    if (!isDirItem(item)) {
      continue;
    }
    if (item.type === 'directory') {
      const entry = new NginxEntry(parent, item.name);
      treeEntrySink(entry);
    }
    if (item.type === 'file') {
      const entry = new NginxBlobEntry(parent, item.name);
      blobEntrySink(entry);
    }
  }
}
