import { createApacheBrowser } from './ApacheBrowser.js';
import { DirPath, FileBrowser } from './FileBrowser.js';
import { createNginxJsonBrowser } from './NginxJsonBrowser.js';

/**
 * Returns the parent URL by stripping the part after the last '/'
 * from URL.pathname
 */
export function parentUrl(url: URL): URL {
  const idx = url.pathname.lastIndexOf('/');
  if (idx < 0) {
    return new URL('/');
  }
  return new URL(url.origin + url.pathname.substring(0, idx + 1));
}

export function allPromises<E, R>(
  x: Iterable<E>,
  f: (e: E) => Promise<R>
): Promise<Awaited<R>[]> {
  const awaited: Promise<R>[] = [];
  for (const e of x) {
    awaited.push(f(e));
  }
  return Promise.all(awaited);
}

export type Func<IN, OUT> = (input: IN) => OUT;
export type BiFunc<IN1, IN2, OUT> = (in1: IN1, in2: IN2) => OUT;
export type Consumer<T> = (t: T) => void;

export class FetchError {
  constructor(readonly statusText: string, readonly url: string) {}
}

export async function fetchRaw(url: string): Promise<string | Response> {
  console.log('fetching ' + url);
  const response = await fetch(url, { redirect: 'follow' });
  if (response.status === 200) {
    return response.text();
  }
  return response;
}

export type ServerTypes = '?' | 'gitea' | 'apache' | 'nginx';
export async function serverType(): Promise<ServerTypes> {
  const response = await fetch('.', {
    method: 'HEAD',
  });
  const server = response.headers.get('server')?.toLowerCase();
  if (server === undefined) {
    return '?';
  }
  if (server.indexOf('nginx') >= 0) {
    return 'nginx';
  }
  if (server.indexOf('codebergpages') >= 0) {
    return 'gitea';
  }
  if (server.indexOf('apache') >= 0) {
    return 'apache';
  }
  return '?';
}

export async function getFileBrowser(
  baseUrl: DirPath
): Promise<FileBrowser | undefined> {
  const st = await serverType();
  console.log('server type is ' + st);
  switch (st) {
    case 'nginx':
      return createNginxJsonBrowser(baseUrl);
    case 'apache':
      return createApacheBrowser(baseUrl);
  }
  return undefined;
}
