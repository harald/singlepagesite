import { DirEntries, DirEntry, DirTreeEntry } from './DirEntry.js';
import { allPromises, Func } from './Util.js';

const CLASS_FOLDED = 'folded';

/**
 * Parameters required in a ToC entry for UI instrumentation.
 */
export class TocEntryFactoryParameters {
  /**
   * Creates the parameter object.
   * @param entryCssClass is the css class to attach to the container element
   * of an individual toc entry to mark it a such
   * @param treeCssClass is the css class to additionally attach to the container
   * if it is a subdirectory entry, i.e. it shall be folded and unfolded.
   */
  constructor(readonly entryCssClass: string, readonly treeCssClass: string) {}
}

export class TocEntryContext {
  constructor(readonly level: number, readonly numElementsOnLevel: number) {}
}

export type TocEntryFactory = (
  d: DirEntry,
  index: number,
  coord: TocEntryContext,
  params: TocEntryFactoryParameters
) => Promise<HTMLElement>;

/**
 * Create a ToC (table of contents) automatically from a provided directory
 * browser.
 */
export class SiteToc {
  private readonly createTocEntry: TocEntryFactory;

  /**
   * Creates the `SiteToc`. The current implementation reads the complete
   * directory tree and creates the respective complete navigation structure.
   * @param dir is a directory browser
   * @param filter to exclude elements an possible sort the list of directory
   * entries on any given level of the directory tree, defaults to the
   * identity function
   * @param displayName derives a user visible name from a directory entry,
   * defaults to {@link DirEntry.DirEntry.name}
   * @param createTocEntry creates an HTML element for a directory entry. The
   * default implementation creates a `div/a[href]` structure.
   */
  constructor(
    private readonly dir: DirTreeEntry,
    private readonly filter: Func<DirEntries, DirEntries> = (x) => x,
    private readonly displayName: Func<DirEntry, string> = (x) => x.name,
    createTocEntry?: Func<DirEntry, Promise<HTMLElement>>
  ) {
    this.createTocEntry = createTocEntry
      ? createTocEntry
      : this.defaultCreateTocEntry;
  }

  /**
   * Creates the whole table of contents inside the given container element.
   */
  create(container: Element, params: TocEntryFactoryParameters): Promise<void> {
    return this.fillInto(container, this.dir, 0, params);
  }

  private async fillInto(
    container: Element,
    dir: DirTreeEntry,
    level: number,
    params: TocEntryFactoryParameters
  ): Promise<void> {
    let entries = await dir.get();
    if (entries === undefined) {
      console.log('sitetoc: nothing found for %o', this.dir);
      return;
    }
    entries = this.filter(entries);
    const context = new TocEntryContext(level, entries.length);
    let i = 0;
    const children = await allPromises(entries, (entry) =>
      this.createTocEntry(entry, i++, context, params)
    );
    container.append(...children);
  }

  private async defaultCreateTocEntry(
    dirEntry: DirEntry,
    index: number,
    context: TocEntryContext,
    params: TocEntryFactoryParameters
  ): Promise<HTMLElement> {
    const tocEntryDiv = document.createElement('div');
    const a = document.createElement('a');
    tocEntryDiv.append(a);
    const pathList = dirEntry.getParentPath();
    pathList.push(dirEntry.name);
    const path = pathList.join('/');
    a.innerHTML = this.displayName(dirEntry);

    if (dirEntry.type === 'blob') {
      a.setAttribute('href', './' + path);
    } else if (dirEntry.type === 'tree') {
      a.setAttribute('href', '#');
      tocEntryDiv.classList.add(params.treeCssClass);
      if (index !== 0 || context.level !== 0) {
        tocEntryDiv.classList.add(CLASS_FOLDED);
      }
      a.onclick = (evt) => this.toggleSubToc(evt, tocEntryDiv);

      const subElem = document.createElement('div');
      tocEntryDiv.append(subElem);
      subElem.classList.add(params.entryCssClass);
      await this.fillInto(subElem, dirEntry, context.level + 1, params);
    } else {
      console.error('this only happens if we added new types for DirEntry');
    }
    return tocEntryDiv;
  }

  private toggleSubToc(evt: Event, toToggle: Element): void {
    evt.preventDefault();
    toToggle.classList.toggle(CLASS_FOLDED);
  }
}
