/**
 * Implements a directory browser providing a root
 * {@link DirEntry.DirEntry} for Gitea git repositories.
 *
 * **Implementation Note:** The gitea api returns json and marks
 * files as `blob` and directories as `tree`.
 * @see [api definition at gitea.io](https://try.gitea.io/api/swagger#/repository/GetTree)
 * @see [example output data](https://codeberg.org/api/v1/repos/harald/singlepagesite/git/trees/main)
 *
 * @module
 */
import { DirPath, FileBrowser } from './FileBrowser.js';
import {
  AbstractDirEntry,
  DirBaseEntry,
  DirBlobEntry,
  DirTreeEntry,
} from './DirEntry';
import { Consumer } from './Util.js';

export class RevProxySpec {
  constructor(
    private readonly proxyPrefix: string,
    private readonly proxiedPrefix: string
  ) {}
  toProxyUrl(proxiedUrl: string): string {
    if (proxiedUrl.startsWith(this.proxiedPrefix)) {
      return this.proxyPrefix + proxiedUrl.substring(this.proxiedPrefix.length);
    }
    return proxiedUrl;
  }
}

export function createGiteaBrowser(
  urlPrefix: string,
  user: string,
  repo: string,
  branch = 'main',
  revProxy: RevProxySpec = DO_NOTHING_PROXY
): FileBrowser {
  const baseUrl: DirPath = `${urlPrefix}/api/v1/repos/${user}/${repo}/git/trees/${branch}/`;
  const converter = (
    parent: DirTreeEntry,
    text: string,
    treeSink: Consumer<DirBaseEntry>,
    blobSink: Consumer<DirBlobEntry>
  ): void => convert(parent, text, treeSink, blobSink, revProxy);
  return new FileBrowser(baseUrl, converter);
}

const DO_NOTHING_PROXY = new RevProxySpec('x', 'x');

interface GiteaTreeItem {
  readonly path: string;
  readonly type: 'blob' | 'tree';
  readonly url: string;
}

function isGiteaTreeItem(obj: object): obj is GiteaTreeItem {
  return (
    typeof obj['path'] === 'string' &&
    typeof obj['url'] === 'string' &&
    ['blob', 'tree'].includes(obj['type'])
  );
}

class GiteaEntry extends AbstractDirEntry {
  constructor(
    parent: DirTreeEntry,
    name: string,
    private readonly url: string
  ) {
    super(parent, name);
  }
  override getUrlPath(): string {
    return this.url;
  }
}

class GiteaBlobEntry extends AbstractDirEntry implements DirBlobEntry {
  readonly type: 'blob' = 'blob';
  constructor(
    parent: DirTreeEntry,
    name: string,
    private readonly url: string
  ) {
    super(parent, name);
  }
  getUrlPath(): string {
    return this.url;
  }
}

function convert(
  parent: DirTreeEntry,
  text: string,
  treeEntrySink: Consumer<DirBaseEntry>,
  blobEntrySink: Consumer<DirBlobEntry>,
  revProxy: RevProxySpec
): void {
  const rawObj = JSON.parse(text);
  if (!('tree' in rawObj) || !Array.isArray(rawObj.tree)) {
    return;
  }
  for (const item of rawObj.tree) {
    if (!isGiteaTreeItem(item)) {
      continue;
    }
    switch (item.type) {
      case 'tree': {
        const url = revProxy.toProxyUrl(item.url);
        const entry = new GiteaEntry(parent, item.path, url);
        treeEntrySink(entry);
        break;
      }
      case 'blob': {
        const url = revProxy.toProxyUrl(item.url);
        const entry = new GiteaBlobEntry(parent, item.path, url);
        blobEntrySink(entry);
        break;
      }
      default:
        throw new Error('failing switch on item.type=' + item.type);
    }
  }
}
