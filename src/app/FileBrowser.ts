import {
  AbstractDirEntry,
  DirBaseEntry,
  DirBlobEntry,
  DirEntries,
  DirEntry,
  DirTreeEntry,
} from './DirEntry.js';
import { Consumer, fetchRaw } from './Util.js';

/**
 * When we need a URL representing a directory rather than a file it should end
 * with a `'/'`. At least nginx and apache like it that way and otherwise first
 * send a redirect to the URL **with** the trailing slash.
 */
export type DirPath = `${string}/`;

class BrowseableDirEntry implements DirTreeEntry {
  readonly type: 'tree' = 'tree';
  parent: DirTreeEntry | undefined;
  constructor(
    private readonly browser: FileBrowser,
    private readonly wrapped: DirBaseEntry
  ) {}
  get name(): string {
    return this.wrapped.name;
  }
  getParentPath(): string[] {
    return this.wrapped.getParentPath();
  }
  getPath(): string[] {
    return this.wrapped.getPath();
  }
  getUrlPath(): string {
    return this.wrapped.getUrlPath();
  }
  get(): Promise<DirEntries | undefined> {
    return this.browser.getDir(this);
  }
}

/**
 * A function to convert text data assumed to contain textual representaions of
 * directory entries into structured data.
 *
 * @param parent is the parent directory entry of all entries produced
 * @param textData is the text to parse directory entries from
 * @param treeEntrySink shall be filled with tree nodes founds
 * @param blobEntrySink shall be filled with the blob nodes found
 */
export interface DirectoryConverter {
  (
    parent: DirTreeEntry,
    textData: string,
    treeEntrySink: Consumer<DirBaseEntry>,
    blobEntrySink: Consumer<DirBlobEntry>
  ): void;
}

export class FileBrowser extends AbstractDirEntry implements DirTreeEntry {
  readonly type: 'tree' = 'tree';
  constructor(
    private readonly baseUrl: DirPath,
    private readonly convert: DirectoryConverter
  ) {
    super(undefined, baseUrl);
  }

  override getUrlPath(): string {
    return this.baseUrl;
  }

  get(): Promise<DirEntries | undefined> {
    return this.getDir(this);
  }

  async getDir(dir: DirTreeEntry): Promise<DirEntries | undefined> {
    const url = dir.getUrlPath();
    const raw = await fetchRaw(url);
    if (raw instanceof Response) {
      console.log('could not retrieve ' + url + ': ' + raw.statusText);
      return undefined;
    }
    const result: DirEntry[] = [];
    this.convert(
      dir,
      raw,
      (t: DirBaseEntry) => this.add(result, t),
      (t: DirBlobEntry) => result.push(t)
    );
    return result;
  }

  private add(result: DirEntry[], dir: DirBaseEntry): void {
    result.push(new BrowseableDirEntry(this, dir));
  }
}
