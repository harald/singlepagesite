export function multiLinebreaksAsParagraph(
  text: string,
  h: Headers
): Promise<string> {
  const contentType = h.get('Content-Type');
  if (contentType !== null && contentType.startsWith('text/html')) {
    return Promise.resolve(text.replace(/(\r?\n){2,}/g, '\n\n<p>'));
  }
  return Promise.resolve(text);
}
