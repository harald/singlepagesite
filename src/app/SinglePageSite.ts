import { TocEntryFactoryParameters } from './SiteToc.js';
import { allPromises, BiFunc, parentUrl } from './Util.js';

/**
 * To be called on an element the children of which are about to be replaced
 * by new content. Introduced to support `MathJax.typesetClear`, but may
 * occasionally be useful elsewhere.
 * @param el the element the children of which are to be disposed. The element
 * itself should rather not be touched and in particular not be removed
 * from the DOM.
 */
export type DisposeElementHandler = (el: Element) => Promise<void>;

/**
 * To be called on text received from the server before it is parsed as HTML.
 * Can be used to make adjustments to the plain text itself or even convert it
 * from something else, say Markdown, to HTML.
 * @param text new text received from the server
 * @param h the headers received from the server, for example to check the
 * Content-Type before applying transformations
 * @return the transformed text
 */
export type NewTextHandler = (text: string, h: Headers) => Promise<string>;

/**
 * To be called after new text from the server was handled by any
 * {@link NewTextHandler} and parsed into HTML.
 * @param el is the element the children of which are freshly parsed, the same
 * that was passed to any {@link DisposeElementHandler}
 * @param h the headers received from the server, for example to check the
 * Content-Type before applying transformations
 */
export type NewElementHandler = (el: Element, h: Headers) => Promise<void>;

const ATTR_USE_SITETOC = 'useSitetoc';
const ATTR_ADD_PREVIEWS = 'addPreviews';
const ATTR_ERROR_MARKER = 'contentLoadError';
const CLASS_MAIN_CONTENT = 'mainContent';
const CLASS_PREVIEW = 'preview';
const CLASS_SITETOC = 'sitetoc';
const CLASS_SITETOC_DIR = 'directory';
const QUERY_MAIN_CONTENT = 'div.' + CLASS_MAIN_CONTENT;
const QUERY_SITE_TOC = `div.${CLASS_SITETOC} a[href]:not(.${CLASS_SITETOC_DIR}):not([target])`;
const TocParams = new TocEntryFactoryParameters(
  CLASS_SITETOC,
  CLASS_SITETOC_DIR
);

export class SinglePageSite {
  private readonly baseUrl: URL;
  private readonly initialPage: string | undefined;
  private readonly onDispose: DisposeElementHandler[] = [];
  private readonly onNewText: NewTextHandler[] = [];
  private readonly onNewElement: NewElementHandler[] = [];
  private siteToc: BiFunc<Element, TocEntryFactoryParameters, Promise<void>> =
    () => {
      console.log('no siteToc defined, skipping retrieval');
      return Promise.resolve();
    };

  /**
   * @param startUrl must be the URL relative to which we're going to load
   * the blog content snippets, in particular those with non-relative URLs. It
   * may contain parameter values, we'll strip them here anyway
   */
  constructor(startUrl?: string) {
    this.baseUrl =
      startUrl !== undefined ? new URL(startUrl) : new URL(location.href);
    this.initialPage = this.baseUrl.searchParams.get('p') || undefined;
    this.baseUrl.search = '';

    console.log('baseUrl=' + this.baseUrl);
  }

  setSiteToc(
    generator: BiFunc<Element, TocEntryFactoryParameters, Promise<void>>
  ): void {
    this.siteToc = generator;
  }

  addDisposeElementHandlers(...onDispose: DisposeElementHandler[]): void {
    this.onDispose.push(...onDispose);
  }

  addNewTextHandlers(...onNewText: NewTextHandler[]): void {
    this.onNewText.push(...onNewText);
  }

  addNewElementHandlers(...onNewElement: NewElementHandler[]): void {
    this.onNewElement.push(...onNewElement);
  }

  async startup(): Promise<void> {
    window.onpopstate = (evt: PopStateEvent) => this.navigateBack(evt);
    const sitetocElem = document.querySelector('.' + CLASS_SITETOC);
    if (sitetocElem === null) {
      return;
    }
    await this.siteToc(sitetocElem, TocParams);
    if (this.initialPage !== undefined) {
      const pUrl = new URL(this.baseUrl);
      pUrl.searchParams.append('p', this.initialPage);
      await this.loadContent(pUrl.toString(), undefined);
    } else {
      this.autoAddPreviews();
      await allPromises(
        document.body.querySelectorAll(QUERY_MAIN_CONTENT),
        (elem) => this.loadPreviews(elem)
      );
    }
    this.instrumentHrefs(document.body, this.baseUrl);
    //document.body.style.display = 'none';
    //await this.wrapTemplates(document.body, new Set<string>());
  }

  /**
   * Add a number of previews as found in site toc to the main content.
   */
  private autoAddPreviews(): void {
    const container = document.body.querySelector(`[${ATTR_ADD_PREVIEWS}]`);
    if (container === null) {
      return;
    }
    let numPreviews = Number(container?.getAttribute(ATTR_ADD_PREVIEWS));
    if (isNaN(numPreviews) || numPreviews <= 0) {
      numPreviews = 3;
    }

    const tocEntries = document.body.querySelectorAll(QUERY_SITE_TOC);
    let n = Math.min(tocEntries.length, numPreviews);
    for (const aHref of tocEntries) {
      if (n === 0) {
        break;
      }
      const href = aHref.getAttribute('href');
      if (href === null || href === '' || href === '#') {
        continue;
      }
      n = n - 1;
      const previewDiv = document.createElement('div');
      previewDiv.setAttribute(ATTR_USE_SITETOC, href);
      previewDiv.classList.add(CLASS_PREVIEW);
      container.append(previewDiv);
    }
  }

  private instrumentHrefs(elem: Element, relativeUrl: URL): void {
    elem.querySelectorAll('a[href]:not([target])').forEach((aHref) => {
      if (aHref instanceof HTMLAnchorElement) {
        this.instrumentHref(aHref, relativeUrl);
      }
    });
  }

  private instrumentHref(aHref: HTMLAnchorElement, relativeBase: URL): void {
    const href = aHref.getAttribute('href');
    if (href === null || href === '' || href === '#') {
      return;
    }
    if (/^https?:[/][/]|^mailto:/.test(href)) {
      return;
    }
    const url = this.resolveRelativeUrl(relativeBase, href);
    const resourcePath = url
      .toString()
      .substring(this.baseUrl.toString().length);
    if (resourcePath === '') {
      aHref.setAttribute('href', this.baseUrl.toString());
    } else {
      const params = new URLSearchParams({ p: resourcePath });
      const pUrl = this.baseUrl + '?' + params;
      aHref.setAttribute('href', pUrl);
      aHref.onclick = (evt) => this.navigateForward(pUrl, evt);
    }
  }

  private resolveRelativeUrl(relativeBase: URL, href: string): URL {
    if (href.startsWith('/')) {
      relativeBase = this.baseUrl;
    }
    return new URL(relativeBase + href);
  }

  private async loadPreviews(content: Element): Promise<void> {
    await allPromises(
      content.querySelectorAll(`div.${CLASS_PREVIEW}[${ATTR_USE_SITETOC}]`),
      async (elem) => {
        const href = elem.getAttribute(ATTR_USE_SITETOC);
        if (href === null) {
          return;
        }
        const url = this.resolveRelativeUrl(this.baseUrl, href);
        await this.loadInto(elem, new URL(url));
        this.linkTitleToArticle(elem, href);
        //console.log('finished inserting for ' + href);
      }
    );
  }

  private linkTitleToArticle(elem: Element, pUrl: string): void {
    const h1 = elem.querySelector('h1.title');
    const aHref = document.createElement('a');
    aHref.setAttribute('href', pUrl);
    if (h1 instanceof HTMLElement) {
      h1.replaceWith(aHref);
      aHref.appendChild(h1);
      return;
    }
    if (elem.hasAttribute(ATTR_ERROR_MARKER)) {
      return;
    }
    aHref.innerHTML = '...article';
    elem.prepend(aHref);
  }

  private straightenUrl(parametrizedUrl: string): URL {
    const url = new URL(parametrizedUrl);
    const params = new URLSearchParams(url.search);
    const resourcePath = params.get('p');
    url.search = '';
    url.pathname += resourcePath;
    return url;
  }

  private async loadContent(
    parametrizedUrl: string,
    evt?: Event
  ): Promise<void> {
    evt?.preventDefault();
    let content = document.body.querySelector(QUERY_MAIN_CONTENT);
    if (content === null) {
      content = document.createElement('div');
      content.classList.add(CLASS_MAIN_CONTENT);
      document.body.append(content);
    }
    const url = this.straightenUrl(parametrizedUrl);
    await this.loadInto(content, url);
  }

  private async navigateForward(
    parametrizedUrl: string,
    evt: Event
  ): Promise<void> {
    await this.loadContent(parametrizedUrl, evt);
    history.pushState(false, '', parametrizedUrl);
  }

  private async navigateBack(evt: PopStateEvent): Promise<void> {
    if (new URL(location.href).searchParams.get('p') === null) {
      // extra weird Javascript to really reload that page
      location.href = location.href;
      return;
    }
    await this.loadContent(location.href, evt);
  }

  private async loadInto(container: Element, url: URL): Promise<void> {
    //console.log('loading %o', url);
    const [divWrapper, responseHeaders] = await this.loadHtml(url);
    const pUrl = parentUrl(url);
    this.instrumentHrefs(divWrapper, pUrl);
    let chain = Promise.resolve();
    this.onDispose.forEach((disposer: DisposeElementHandler) => {
      chain = chain.then(() => disposer(container));
    });
    await chain;

    container.replaceChildren(...divWrapper.childNodes);
    const e = divWrapper.getAttribute(ATTR_ERROR_MARKER);
    if (e !== null) {
      container.setAttribute(ATTR_ERROR_MARKER, e);
    }
    this.onNewElement.forEach((handler: NewElementHandler) => {
      chain = chain.then(() => handler(container, responseHeaders));
    });
    await chain;
    //await this.onNewElement(container);
  }

  private async loadHtml(src: URL): Promise<[HTMLDivElement, Headers]> {
    const response: Response = await fetch(src);
    const result = document.createElement('div');
    if (response.ok) {
      let text = response.text();
      this.onNewText.forEach((fn: NewTextHandler) => {
        text = text.then((t) => fn(t, response.headers));
      });
      result.innerHTML = await text;
      return [result, response.headers];
    }

    // insert empty error span as a marker only
    result.setAttribute(ATTR_ERROR_MARKER, '');
    result.innerHTML = `Could not load
    <blockquote>${src},</blockquote>
    server returns code
    <code>${response.status} &mdash; ${response.statusText}</code>.`;
    return [result, response.headers];
  }
}
