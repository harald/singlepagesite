/**
 * Definitions for a simple directory entry denoting either a blob (file, leaf node)
 * or a tree (directory, inner node).
 *
 * @module
 */

// export type DirType<T> = T extends 'blob'
//   ? 'blob'
//   : T extends 'tree'
//   ? 'tree'
//   : never;

export type DirType = 'blob' | 'tree';

/**
 * The type of element contained in a `tree` directory entry.
 */
export type DirEntries = ReadonlyArray<DirEntry>;

/**
 * A general directory entry.
 *
 * @param T is the type of the directory entry
 */
export interface DirBaseEntry {
  /**
   * Technical name of this directory entry or file, may or may not be used for
   * display purposes. This should be the part of the URL that specifies this
   * entry without any slashes.
   **/
  readonly name: string;

  /** Parent of this entry, if any. */
  readonly parent: DirTreeEntry | undefined;

  /**
   * Array of the names of all ancestors, empty if this entry has no parent,
   * starting with the root element.
   */
  getParentPath(): string[];

  getPath(): string[];

  getUrlPath(): string;
}

/**
 * Implements most of the {@link DirEntry} interface.
 */
export abstract class AbstractDirEntry implements DirBaseEntry {
  constructor(
    readonly parent: DirTreeEntry | undefined,
    readonly name: string
  ) {}

  getParentPath(): string[] {
    if (this.parent === undefined) {
      return [];
    }
    const result = this.parent.getParentPath();
    result.push(this.parent.name);
    return result;
  }

  getPath(): string[] {
    const pp = this.getParentPath();
    pp.push(this.name);
    return pp;
  }

  abstract getUrlPath(): string;
}

export interface DirBlobEntry extends DirBaseEntry {
  readonly type: 'blob';
}

export interface DirTreeEntry extends DirBaseEntry {
  readonly type: 'tree';
  /**
   * Returns the content described by this inner entry, may be empty.
   * @return `undefined` if problems arise in retrieving the entries.
   */
  get(): Promise<DirEntries | undefined>;
}
export type DirEntry = DirBlobEntry | DirTreeEntry;
