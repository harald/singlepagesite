# Single Page Site


This is an experiment for a minimal static website powered by
Typescript (Javascript).

## Design Goals
* static site, meaning a web server able to deliver files should suffice to run
  it, which in particular is true for the *pages* on github, gitlab, gitea
* no supposedly "simple", home brewn content syntax, use HTML but reduce
  boilerplate as much as possible
* allow some way to use Markdown or similar nevertheless, but that requires
  rendering in the browser, since the server cannot do it

## What is it?
* A small Typescript application (provided here).

## What do I need to provide?
* An `index.html` with a certain structure, see below.
* A web server which is able to serve files, nothing else.
* Blog content as content-only HTML pages, no `<head>` required.

## --------------------------
**Outdated stuff below, more details tbd**

Note that all HTML files except `index.html` should not contain an HTML header or a wrapping `<body>` element, because they will be read and inserted into `index.html` when the user navigates to them.

Given the compiled Javascript is bound into the `index.html` as
described below, it provides the following functionality:

## Rewrite local urls
All URLs which don't start with `http://` or `https://` will be rewritten
such that they work in the context path of `index.html`. This allows to have
an arbitrary root path on the server where you actually serve your static site, say `https://some.host.com/MyBlog/index.html`. Your `index.html` and the other files of your site next to
it or in sub-directories may use URLs like `/2022-07/bogobom.html` with no mention of the full URL in front of `/index.html`. And of course relative URLs work too.

## Load Previews of a number of pages
This requires two things in your `index.html`:

1) An HTML element with the attribute `addPreviews`, the *container* element.
2) A `<div>` with css class `sitetoc` containing `<a>` elements with an `href` attribute.

The the HTML files pointed to by the `href` attributes are loaded into `<div>` elements with css class `preview` which are prepended to the *container* element.

If the value of the attribute `addPreviews` is a number greater zero, it replaces the default of 3 previews to load.

## Navigate and load pages
When rewriting local URLs as described above, they are actually rewritten into URL query parameters of the form `?p=bogobom.html`. So instead of letting the browser do the navigation from page to page, the Javascript is responsible for the navigation. The sole purpose of this is that the content pages do not need all that boilderplate HTML code. They may just concentrate on the content itself.

For this to work, a `<div>` element with class `mainContent` must exist, because this is the one that will be cleared first and then get the trunk HTML page inserted.

Usually the `<div>` with the `addPreviews` attribute and the `mainContent` css class should be the same, thereby replacing the preview with the content of specific pages.


## Hooks called when loading pages
There are two functions which, if available, are called before and after loading a new content page.

```typescript
type Instrumenter = (el: Element) => Promise<void>;
disposedContent: Instrumenter
newContent: Instrumenter
```

Both will be called with the same element, first with the content that will soon be removed from that element and then after new content was added to it.

The functions may work with the child elements of the provided element but should rather not remove the provided element from the DOM.

Both functions were specifically introduced to support MathJax, but may serve other or additional purposes. For MathJax `index.html` could contain the following code:
```javascript
function disposedContent(disposedEl) {
  return Promise.resolve(MathJax.typesetClear([disposedEl]));
}
function newContent(el) {
  MathJax.texReset(1);
  return MathJax.typesetPromise([el]);
}
```

